package cn.edu.bupt.ch3_2.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import cn.edu.bupt.ch3_2.Entity.Views;
import java.util.Date;
@Data
public class SecondPerson {
    private Integer id;
    @JsonProperty("person-name")
    private String name;
    @JsonIgnore
    private Integer age;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @JsonView(Views.Public.class)
    private String nickName;

    @JsonView(Views.Internal.class)
    private String idCard;

    public SecondPerson(String nickName, String idCard) {
        this.nickName = nickName;
        this.idCard = idCard;
    }
}
