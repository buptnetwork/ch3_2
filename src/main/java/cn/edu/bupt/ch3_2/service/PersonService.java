package cn.edu.bupt.ch3_2.service;

import cn.edu.bupt.ch3_2.Entity.SecondPerson;
import org.springframework.stereotype.Service;

public interface PersonService {
    public SecondPerson getPersonById(Integer id);
}
