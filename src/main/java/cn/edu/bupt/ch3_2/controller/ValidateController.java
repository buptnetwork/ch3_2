package cn.edu.bupt.ch3_2.controller;

import cn.edu.bupt.ch3_2.Entity.Person;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Tag(name = "ValidateController")
@RestController
public class ValidateController {
    @Operation(description = "一个validate测试API", summary = "返回成功或者错误")
    @PostMapping("/validate")
    public ResponseEntity validate(@Valid @RequestBody Person person,
                                   BindingResult result){
        if(result.hasErrors()){
            return ResponseEntity.badRequest()
                    .body(result.getAllErrors());
        }
        return ResponseEntity.ok().body("ok");
    }

}
