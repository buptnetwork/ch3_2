package cn.edu.bupt.ch3_2.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@Tag(name = "UploadController")
@RestController
public class UploadController {
    @Value("${uploadFile.location}")
    private String uploadFileLocation;
    //上传文件保存的本地目录，使用@Value获取全局配置文件中配置的属性值

    @Operation(description = "一个上传文件API", summary = "返回值为文件名")
    @PostMapping("/upload")
    public String upload(MultipartFile file) throws IOException {
        if (file == null || file.isEmpty()) {
            return "上传文件为空...";
        }
        String fileName = file.getOriginalFilename();
        File saveFile = new File(uploadFileLocation, fileName);
        file.transferTo(saveFile);//文件保存
        return fileName;
    }

    @Operation(description = "一个上传多张图片API", summary = "返回值为图片文件名")
    @PostMapping("/uploads")
    public String uploads(@RequestParam Map<String, MultipartFile> fileMap) throws IOException {
        StringBuilder info = new StringBuilder();
        fileMap.forEach((key,file) ->{
            info.append(file.getOriginalFilename());
        });
        return info.toString();
    }

}
