package cn.edu.bupt.ch3_2.controller;

import cn.edu.bupt.ch3_2.Entity.SecondPerson;
import cn.edu.bupt.ch3_2.Entity.Views;
import cn.edu.bupt.ch3_2.service.PersonService;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@Tag(name = "JsonController")
@RestController
public class JsonController {
    final
    PersonService personService;

    @Autowired
    public JsonController(PersonService personService) {
        this.personService = personService;
    }

    @Operation(description = "一个JSON定制演示API", summary = "返回值为原JSON")
    @PostMapping("/json")
    public SecondPerson jsonOut(@RequestBody SecondPerson person){
        return person;
    }

    @Operation(description = "jsonView demo", summary = "返回public View")
    @GetMapping("/public")
    @JsonView(Views.Public.class)
    public SecondPerson publicView(){
        return personService.getPersonById(1);
    }

    @Operation(description = "jsonView demo", summary = "返回public View")
    @GetMapping("/internal")
    @JsonView(Views.Internal.class)
    public SecondPerson internalView(){
        return personService.getPersonById(1);
    }
}
