package cn.edu.bupt.ch3_2.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "ExceptionController")
@RestController
public class ExceptionController {
    @Operation(description = "除法计算API", summary = "返回值为商的整数部分，当除数为0时返回400状态码")
  //  @ApiImplicitParams({@ApiImplicitParam(name = "a", value = "被除数a", required = true),
  //          @ApiImplicitParam(name = "b", value = "除数b", required = true),
  //  })
   // @Operation({@ApiResponse(code = 400, message = "除数不能为0")})
    @GetMapping(value = "/div/{a}/{b}")
    public ResponseEntity<String> div(@Parameter @PathVariable Long a, @PathVariable Long b) {
        Long div = a / b;
        return new ResponseEntity<String>("the div is：" + div, HttpStatus.OK);

    }
}
