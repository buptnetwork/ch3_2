package cn.edu.bupt.ch3_2.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//本测试使用@SpringBootTest创建测试环境
@SpringBootTest
//使用此注解后会在测试环境中自动配置MockMvc
@AutoConfigureMockMvc
class JsonControllerATest {

    //注入一个mockMvc用来测试controller
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        System.out.println("BeforeEach......");
    }

    @AfterEach
    void tearDown() {
        System.out.println("AfterEach......");
    }

    @Test
    void publicView() throws Exception {
        //使用mockMvc发起请求
        this.mockMvc
                .perform(get("/public")
                    .accept(MediaType.APPLICATION_JSON))
                //断言，通过的条件1:状态码为200
                .andExpect(status().isOk())
                //断言，通过的条件2:返回的JSON中的nickName值为"ABC"
                .andExpect(jsonPath("$.nickName")
                        .value("ABC"))
                //断言，通过的条件3:返回的JSON中没有idCard
                .andExpect(jsonPath("$.idCard")
                        .doesNotExist());
    }

    @Test
    void internalView() throws Exception {
        //使用mockMvc发起请求
        this.mockMvc.perform(get("/internal")
                    .accept(MediaType.APPLICATION_JSON))
                //断言，通过的条件1:状态码为200
                .andExpect(status().isOk())
                //断言，通过的条件2:返回的JSON中的nickName值为"ABC"
                .andExpect(jsonPath("$.nickName")
                        .value("ABC"))
                //断言，通过的条件3:返回的JSON中的idCard值为"110101199001011234"
                .andExpect(jsonPath("$.idCard")
                        .value("110101199001011234"));

    }
}