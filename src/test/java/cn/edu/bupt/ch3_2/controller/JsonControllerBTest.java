package cn.edu.bupt.ch3_2.controller;

import cn.edu.bupt.ch3_2.Entity.SecondPerson;
import cn.edu.bupt.ch3_2.service.PersonService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//WebMvcTest仅对组件进行测试，不注入依赖组件，需要自己使用MockBean注入
@WebMvcTest//(JsonController.class)
class JsonControllerBTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonService personService;

    @BeforeEach
    void setUp() {
        System.out.println("BeforeEach......");
        given(this.personService.getPersonById(1))
                .willReturn(new SecondPerson("XYZ","110101199001011234"));
    }

    @AfterEach
    void tearDown() {
        System.out.println("AfterEach......");
    }

    @Test
    void publicView() throws Exception {
        this.mockMvc.perform(get("/public").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nickName").value("XYZ"))
                .andExpect(jsonPath("$.idCard").doesNotExist());
    }

    @Test
    void internalView() throws Exception {
        this.mockMvc.perform(get("/internal").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nickName").value("XYZ"))
                .andExpect(jsonPath("$.idCard").value("110101199001011234"));

    }
}